## This Module Developed by Amit Joshi.

> For more help please contact me
> Email:amitkjoshi88@gmail.com

### HOW TO USE ZOOMNOW MODULE: ###

* FOR NORMAL USERS
* In your content type, add an image field.
* Go to "Manage display" then change display format from "Image" to "zoomnow".
* Save your setting. Create a new node (of that content type).
* Upload an image then save it.

# Note: This plugin will work higher verson of jquery minimum 1.7
