<?php

/**
 * @file
 * Adds a tpl file for zoomnow.
 */
$optinsNow = array(
  'zoomnow_option_selected' => variable_get('zoomnow_option_selected', 'hover'),
);
drupal_add_js(array('zoomnow_selected_option' => $optinsNow), 'setting');
?>
<div class="zoomnow">
    <?php $imagedisplay = file_create_url($file['uri']); ?>
    <img src="<?php print $imagedisplay; ?>"/>
</div>
